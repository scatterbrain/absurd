const gulp = require('gulp'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  sourcemaps = require('gulp-sourcemaps'),
  babel = require('gulp-babel'),
  pug = require('gulp-pug'),
  imagemin = require('gulp-imagemin'),
  del = require('del'),
  gulpif = require('gulp-if'),
  uglify = require('gulp-uglify'),
  cleanCSS = require('gulp-clean-css'),
  htmlmin = require('gulp-htmlmin'),
  browserSync = require('browser-sync').create(),
  runSequence = require('run-sequence');

let dev_env = true; // used to change paths/run tasks conditionally

const basePaths = {
  scss: 'src/assets/scss',
  js: 'src/assets/js',
  imgs: 'src/assets/imgs',
  html: 'src/templates',
  fonts: 'src/assets/fonts'
}

gulp.task('set_prod', function () {
  dev_env = false
});

gulp.task('set_dev', function () {
  dev_env = true
});

// styles
gulp.task('styles', function () {
  const destPath = dev_env ? '.tmp/assets/css/' : 'dist/assets/css/'
  return gulp.src(`${basePaths.scss}/**/*.scss`)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 2 versions'],
      cascade: false
    }))
    // create sourcemaps if in dev mode
    .pipe(gulpif(dev_env, sourcemaps.write('./')))
    // if not dev_mode minify css
    .pipe(gulpif(!dev_env,
      cleanCSS({
        compatibility: 'ie10'
      })
    ))
    .pipe(gulp.dest(destPath))
    .pipe(browserSync.reload({
      stream:true
    }))
});

// scripts
gulp.task('scripts', function () {
  const destPath = dev_env ? '.tmp/assets/js/' : 'dist/assets/js/'
  return gulp.src(`${basePaths.js}/**/*.js`)
    .pipe(sourcemaps.init())
    .pipe(babel({
      presets: ['es2015']
    }))
    // create sourcemaps if in dev mode
    .pipe(gulpif(dev_env, sourcemaps.write('./')))
    // if not dev_mode minify js
    .pipe(gulpif(!dev_env, uglify()))
    .pipe(gulp.dest(destPath))
    .pipe(browserSync.reload({
      stream:true
    }))
});

// images
gulp.task('images', function () {
  const destPath = dev_env ? '.tmp/assets/imgs/' : 'dist/assets/imgs/'
  return gulp.src(`${basePaths.imgs}/**/*`)
    .pipe(imagemin())
    .pipe(gulp.dest(destPath))
    .pipe(browserSync.reload({
      stream: true
    }))
});

// fonts
gulp.task('fonts', function () {
  const destPath = dev_env ? '.tmp/assets/fonts/' : 'dist/assets/fonts/'
  return gulp.src(`${basePaths.fonts}/**/*`)
    .pipe(gulp.dest(destPath))
});

// html
gulp.task('html', function () {
  const destPath = dev_env ? '.tmp/' : 'dist/'
  return gulp.src(`${basePaths.html}/pages/**/*.pug`)
    .pipe(pug({}))
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(gulp.dest(destPath))
    .pipe(browserSync.reload({
      stream: true
    }))
});

// browersync server
gulp.task('server', ['styles', 'scripts', 'images', 'fonts', 'html'], function () {
  browserSync.init({
    server: {
      baseDir: '.tmp'
    },
    injectChanges: true,
    notify: false,
    ghostMode: false,
  });
  gulp.watch(`${basePaths.scss}/**/*.scss`, ['styles']);
  gulp.watch(`${basePaths.js}/**/*.js`, ['scripts']);
  gulp.watch(`${basePaths.imgs}/**/*`, ['images']);
  gulp.watch(`${basePaths.html}/**/*.pug`, ['html']);
});

// bin off relevant folder before building 
gulp.task('clean', function () {
  const folderToDelete = dev_env ? '.tmp' : 'dist'
  return del([
    folderToDelete
  ]);
});

gulp.task('default', function () {
  runSequence('set_dev', 'clean', ['server']);
});

gulp.task('build', function () {
  runSequence('set_prod', 'clean', ['styles', 'scripts', 'images', 'fonts', 'html']);
});
